package com.lwise.javabasic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaBasicApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaBasicApplication.class, args);
        boolean myBox1 = true;
        System.out.println(myBox1);
        char myBox2 = 'A';
        Character myBox222 ='A';
        String myBox22 ="aa";

        byte myBox3 = 1;
        Byte myBox33 = 1;

        short myBox4 = 2;
        int myBox5 = 30;
        long myBox6 = 6L;

        float myBox7 = 11.2f;
        double myBox8 = 12.22478;
    }

}
